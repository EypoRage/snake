﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class Snake : MonoBehaviour
{
    //****** by Tobias Krammer ******
    // Current Movement Direction
    // (by default it moves to the right)
    Vector2 dir = Vector2.right;

    // Keep Track of Tail
    List<Transform> tail = new List<Transform>();

    // Tail Prefab
	public GameObject tailPrefab;
    public GameObject tailPrefabStandard;
	public GameObject tailPrefabGreen;
	public GameObject tailPrefabRed;
	public GameObject tailPrefabBlue;



    // Did the snake eat something?
    bool ate = false;


    //pivates
    private Vector2 touchOrigin = -Vector2.one; //Used to store location of screen touch origin for mobile controls.
    

    //publics
    public float movementFactor = 0.3f;
    public static double score = 0;
    public double time = 0;
    public double pointsPerFood = 1;
    
    public Text scoreText;
    public Text livingTime;
    public AudioClip audioPickup;
    AudioSource audioData;

    // Use this for initialization
	void Start(){

	GameObject background = GameObject.Find("Background");
	GameObject snakeg = GameObject.Find("snakePrefab");

    GameObject borderLeft = GameObject.Find("Worldborders/BorderLeft");
    GameObject borderRight = GameObject.Find("Worldborders/BorderRight");
    GameObject borderTop = GameObject.Find("Worldborders/BorderTop");
    GameObject borderBottom = GameObject.Find("Worldborders/BorderBottom");

        //initialize settings
        switch (Settings.mode) {
		case "Easy":
			movementFactor = 0.1f;
			break;
		case "Middle":
			movementFactor = 0.07f;
			break;
		case "Hard":
			movementFactor = 0.05f;
			break;

		default:
			movementFactor = 0.07f;
			break;
		}


		switch (Settings.snake) {
		case "Standard":
			tailPrefab = tailPrefabStandard;
			break;
		case "Green":
			snakeg.GetComponent<SpriteRenderer> ().color = new Color (0f,1f,0f,1f);
			tailPrefab = tailPrefabGreen;
			break;
		case "Red":
			snakeg.GetComponent<SpriteRenderer> ().color = new Color (1f,0f,0f,1f);
			tailPrefab = tailPrefabRed;
			break;
		case "Blue":
			snakeg.GetComponent<SpriteRenderer> ().color = new Color (0f,0f,1f,1f);
			tailPrefab = tailPrefabBlue;
			break;

		default:
			Debug.Log("Kein Wert gefunden");
			break;
		}


		switch (Settings.map) {
            case "Standard":
                borderLeft.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
                borderRight.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
                borderTop.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
                borderBottom.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

                break;
            case "Green":
                borderLeft.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 1f);
                borderRight.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 1f);
                borderTop.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 1f);
                borderBottom.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 1f);
                break;
            case "Red":
                borderLeft.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
                borderRight.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
                borderTop.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
                borderBottom.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
                break;
            case "Blue":
                borderLeft.GetComponent<SpriteRenderer>().color = new Color(0f, 0f, 1f, 1f);
                borderRight.GetComponent<SpriteRenderer>().color = new Color(0f, 0f, 1f, 1f);
                borderTop.GetComponent<SpriteRenderer>().color = new Color(0f, 0f, 1f, 1f);
                borderBottom.GetComponent<SpriteRenderer>().color = new Color(0f, 0f, 1f, 1f);
                break;

            default:
                Debug.Log("Kein Wert gefunden");
                break;
        }

		Debug.Log (background.activeSelf);

		Debug.Log (tailPrefab);

        audioData = GetComponent<AudioSource>();

        score = 0;
    
    //Find the UI Score Text and initiallize it to 0
    scoreText = GameObject.Find("tScore").GetComponent<Text>();
        scoreText.text = "SCORE: " + score.ToString();

        //Find the UI Time Text, initiallize it to 0 
        livingTime = GameObject.Find("tTime").GetComponent<Text>();
        livingTime.text = "TIME: " + Time.timeSinceLevelLoad.ToString("f2");


        // Move the Snake every 300ms
        InvokeRepeating("Move", movementFactor, movementFactor);
    }

    // Update is called once per frame
    void Update()
    {

    

#if  UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER

        // Move in a new Direction?
        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (score != 0 && dir != Vector2.left) dir = Vector2.right;
            else if (score == 0) dir = Vector2.right;
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            if (score != 0 && dir != Vector2.up) dir = Vector2.down;
            else if (score == 0) dir = Vector2.down;
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (score != 0 && dir != Vector2.right) dir = Vector2.left;
            else if (score == 0) dir = Vector2.left;
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            if (score != 0 && dir != Vector2.down) dir = Vector2.up;
            else if (score == 0) dir = Vector2.up;
        }


#else
        int horizontal = 0;
        int vertical = 0;
        //Check if Input has registered more than zero touches
        if (Input.touchCount > 0)
        {
            //Store the first touch detected.
            Touch myTouch = Input.touches[0];

            //Check if the phase of that touch equals Began
            if (myTouch.phase == TouchPhase.Began)
            {
                //If so, set touchOrigin to the position of that touch
                touchOrigin = myTouch.position;
            }

            //If the touch phase is not Began, and instead is equal to Ended and the x of touchOrigin is greater or equal to zero:
            else if (myTouch.phase == TouchPhase.Ended && touchOrigin.x >= 0)
            {
                //Set touchEnd to equal the position of this touch
                Vector2 touchEnd = myTouch.position;

                //Calculate the difference between the beginning and end of the touch on the x axis.
                float x = touchEnd.x - touchOrigin.x;

                //Calculate the difference between the beginning and end of the touch on the y axis.
                float y = touchEnd.y - touchOrigin.y;

                //Set touchOrigin.x to -1 so that our else if statement will evaluate false and not repeat immediately.
                touchOrigin.x = -1;

                //Check if the difference along the x axis is greater than the difference along the y axis.
                if (Mathf.Abs(x) > Mathf.Abs(y))
                    //If x is greater than zero, set horizontal to 1, otherwise set it to -1
                    horizontal = x > 0 ? 1 : -1;
                else
                    //If y is greater than zero, set horizontal to 1, otherwise set it to -1
                    vertical = y > 0 ? 1 : -1;
            }
        }

        /* Move in a new Direction?
        if (horizontal ==1)
            dir = Vector2.right;
        else if (vertical==-1)
            dir = -Vector2.up;    // '-up' means 'down'
        else if (horizontal==-1)
            dir = -Vector2.right; // '-right' means 'left'
        else if (vertical==1)
            dir = Vector2.up;
        */

       

               if (horizontal==1)
        {
            if (score != 0 && dir != Vector2.left) dir = Vector2.right;
            else if (score == 0) dir = Vector2.right;
        }
        else if (vertical==-1)
        {
            if (score != 0 && dir != Vector2.up) dir = Vector2.down;
            else if (score == 0) dir = Vector2.down;
        }
        else if (horizontal==-1)
        {
            if (score != 0 && dir != Vector2.right) dir = Vector2.left;
            else if (score == 0) dir = Vector2.left;
        }
        else if (vertical==1)
        {
            if (score != 0 && dir != Vector2.down) dir = Vector2.up;
            else if (score == 0) dir = Vector2.up;
        }

#endif //End of mobile platform dependendent compilation section started above with #elif





        //Get the current time of the game
        livingTime.text = "TIME: " + Time.timeSinceLevelLoad.ToString("f2");


    }

    void Move()
    {
        // Save current position (gap will be here)
        Vector2 v = transform.position;

        // Move head into new direction
        transform.Translate(dir);
 
        // Ate something? Then insert new Element into gap
        if (ate)
        {
            //play pickup sound
            audioData.PlayOneShot(audioPickup);

            // Load Prefab into the world
            GameObject g = (GameObject)Instantiate(tailPrefab, v, Quaternion.identity);

            // Keep track of it in our tail list
            tail.Insert(0, g.transform);

            // Reset the flag
            ate = false;
        }
        // Do we have a Tail?
        else if (tail.Count > 0)
        {
            // Move last Tail Element to where the Head was
            tail.Last().position = v;

            // Add to front of list, remove from the back
            tail.Insert(0, tail.Last());
            tail.RemoveAt(tail.Count - 1);
        } 
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        // Food?
        if (coll.name.StartsWith("FoodPrefab"))
        {
            // Get longer in next Move call
            ate = true;

            // Remove the Food
            Destroy(coll.gameObject);

            //Add points to score
            score = score + pointsPerFood;

            scoreText.text = "SCORE: " + score.ToString();

            Debug.Log("Ate food");

        }

        //powerup?
        else if (coll.name.StartsWith("PowerupPrefab"))
        {
            // Get longer in next Move call
            ate = true;

            // Remove the Food
            Destroy(coll.gameObject);


            //get random Powerup
            int p = Random.Range(1, 4);

            switch (p) {

                case 1:
                    //Add points to score
                    score = score + 3* pointsPerFood;

                    scoreText.text = "SCORE: " + score.ToString();

                    break;

                case 2:

                    score = score - 3 * pointsPerFood;

                    scoreText.text = "SCORE: " + score.ToString();

                    break;

                case 3:
                   movementFactor = movementFactor + 0.5f;
                    break;

                case 4:
                    movementFactor = movementFactor - 0.5f;
                    break;
            }

            //Add points to score
            score = score + pointsPerFood;

            scoreText.text = "SCORE: " + score.ToString();

            Debug.Log("Ate Powerup");

        }



        // Collided with Tail or Border
        else
        {
            Debug.Log("Triggered!!!!!");

            //Change the game state
            GameController.gamestate = 3; //Lost-State

            

        }
    }

}
