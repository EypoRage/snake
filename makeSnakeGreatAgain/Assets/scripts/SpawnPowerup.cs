﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPowerup : MonoBehaviour {



    //****** by Tobias Krammer ******

    //Food Prefab
    public GameObject powerupPrefab;

    //Borders positions
    public Transform BorderTop;
    public Transform BorderBottom;
    public Transform BorderLeft;
    public Transform BorderRight;

    // Use this for initialization
    void Start () {
        //initialize settings
        switch (Settings.boni)
        {
            case "ON":
                InvokeRepeating("Spawn", 10, 10);
                break;
            case "OFF":
                break;
        }

        
       
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void Spawn()
    {
        // x position between left & right border
        int x = (int)Random.Range(BorderLeft.position.x, BorderRight.position.x);

        // y position between top & bottom border
        int y = (int)Random.Range(BorderBottom.position.y, BorderTop.position.y);

        // Instantiate the food at (x, y)
        Instantiate(powerupPrefab,
                    new Vector2(x, y),
                    Quaternion.identity); // default rotation
    }


    

}
