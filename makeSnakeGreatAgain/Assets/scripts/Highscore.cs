﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class Highscore : MonoBehaviour {


    //****** Tobias Krammer ****** init List

   public static List<string> highscores = new List<string>();
    public Text textObject;



	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update() 
     
    {
        // textObject.GetComponent<Text>().text = highscore1;

        StringBuilder builder = new StringBuilder();
        foreach (string score in highscores) // Loop through all strings
        {
            builder.Append(score).Append("\n"); // Append string to StringBuilder
        }
        string result = builder.ToString(); // Get string from StringBuilder
        Debug.Log("Highscore");
        Debug.Log("Result:" + result);

        textObject.text = result;

    }
}


  

