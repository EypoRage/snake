﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    /*
     * Defineing gamestate: is used for looking in what gamephase the game is
     * 1. Programm just started -> Show initial ui text
     * 2. Game is running -> listen for changes in the game state
     * 3. Game is lost -> do the loosing Text and perform ui controlls
     * 
     * Use this Class for most Ui and general game behaviour!!!
     */

    public static int gamestate;



    public GameObject tPlay;
    public GameObject tLoose;

    public GameObject inputName;
    public Text name;


    // Use this for initialization
    void Start () {
        gamestate = 1; //start
       
    }
	
	// Update is called once per frame
	void Update () {

        if (gamestate == 1)
        {
            //Stop the Game
           if(Time.timeScale!=0) Time.timeScale = 0;

            if (Input.anyKeyDown) {
                gamestate = 2;
                tPlay.SetActive(false);
                }

        }
        else if (gamestate == 2)
        {
            //Run Forest run!
            if (Time.timeScale != 1) Time.timeScale = 1;
        }
        else if (gamestate == 3) {
            //Stop the game
            if (Time.timeScale!=0) Time.timeScale = 0;
            
            inputName.SetActive(true);

            tLoose.SetActive(true);

            //if(Input.anyKeyDown) Application.LoadLevel(Application.loadedLevel);

        }
	}



    public void replay()
    {
        

        string highscore = name.text + " - " + Snake.score.ToString();

        Highscore.highscores.Add(highscore);
        Application.LoadLevel(Application.loadedLevel);
       
    }

}
