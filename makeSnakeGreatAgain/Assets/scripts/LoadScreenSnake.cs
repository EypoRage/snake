﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class LoadScreenSnake : MonoBehaviour
{
    // Current Movement Direction
    // (by default it moves to the right)
    Vector2 dir = Vector2.right;

    // Keep Track of Tail
    List<Transform> tail = new List<Transform>();

    // Tail Prefab
    public GameObject tailPrefab;

    // Did the snake eat something?
    bool ate = false;


    //pivates
    private Vector2 touchOrigin = -Vector2.one; //Used to store location of screen touch origin for mobile controls.


    //publics
    public float movementFactor = 0.3f;
    public double score = 0;
    public double time = 0;
    public double pointsPerFood = 1;

    public Text scoreText;
    public Text livingTime;
    public AudioClip audioPickup;
    AudioSource audioData;

    // Use this for initialization
    void Start()
    {

        


        // Move the Snake every 300ms
        InvokeRepeating("Move", movementFactor, movementFactor);
    }

    // Update is called once per frame
    void Update()
    {

        dir = Vector2.right;

    }

    void Move()
    {
        // Save current position (gap will be here)
        Vector2 v = transform.position;

        // Move head into new direction
        transform.Translate(dir);

        // Ate something? Then insert new Element into gap
        if (ate)
        {
            
            // Load Prefab into the world
            GameObject g = (GameObject)Instantiate(tailPrefab, v, Quaternion.identity);

            // Keep track of it in our tail list
            tail.Insert(0, g.transform);

            // Reset the flag
            ate = false;
        }
        // Do we have a Tail?
        else if (tail.Count > 0)
        {
            // Move last Tail Element to where the Head was
            tail.Last().position = v;

            // Add to front of list, remove from the back
            tail.Insert(0, tail.Last());
            tail.RemoveAt(tail.Count - 1);
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        // Food?
        if (coll.name.StartsWith("FoodPrefab"))
        {
            // Get longer in next Move call
            ate = true;

            // Remove the Food
            Destroy(coll.gameObject);

     

            Debug.Log("Ate food");

        }
       
    }

}
