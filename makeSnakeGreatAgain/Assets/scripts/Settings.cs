﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class Settings : MonoBehaviour {


	//****** by Tobias Krammer****** using generics n stuff , too tired to explain this fancy stuff....

	public static string mode;
	public static string map;
	public static string snake;
	public static string boni;


	public GameObject dropdownMode;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {



	}


	public void switchmode(){

		dropdownMode = GameObject.Find ("Mode");

		int menuindex = dropdownMode.GetComponent<Dropdown> ().value;
		List<Dropdown.OptionData> menuOptions = dropdownMode.GetComponent<Dropdown> ().options;
		string value = menuOptions [menuindex].text;
		mode = value;
		Debug.Log (value);

	}

	public void switchmap(){
		dropdownMode = GameObject.Find ("Map");

		int menuindex = dropdownMode.GetComponent<Dropdown> ().value;
		List<Dropdown.OptionData> menuOptions = dropdownMode.GetComponent<Dropdown> ().options;
		string value = menuOptions [menuindex].text;
		map = value;
		Debug.Log (value);
	}


	public void switchsnake(){
		dropdownMode = GameObject.Find ("Snake");

		int menuindex = dropdownMode.GetComponent<Dropdown> ().value;
		List<Dropdown.OptionData> menuOptions = dropdownMode.GetComponent<Dropdown> ().options;
		string value = menuOptions [menuindex].text;
		snake = value;
		Debug.Log (value);
	}

	public void switchboni(){
		dropdownMode = GameObject.Find ("Boni");

		int menuindex = dropdownMode.GetComponent<Dropdown> ().value;
		List<Dropdown.OptionData> menuOptions = dropdownMode.GetComponent<Dropdown> ().options;
		string value = menuOptions [menuindex].text;
		boni = value;
		Debug.Log (value);
	}

}
