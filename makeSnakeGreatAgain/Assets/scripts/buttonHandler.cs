﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class buttonHandler : MonoBehaviour
{


    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }


   public void OpenLink(string link)
    {
        Application.OpenURL(link);
    }



    public void SwitchScene(string sceneName)
    {
        SceneManager.LoadScene("LoadingScene");

        LoadNextScene.sceneName = sceneName;

    }


}

