﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkSpawnFood : NetworkBehaviour {

    //Food Prefab
    public GameObject foodPrefab;

    //Borders positions
    public Transform BorderTop;
    public Transform BorderBottom;
    public Transform BorderLeft;
    public Transform BorderRight;

    // Use this for initialization
    void Start () {
        // Spawn food every 4 seconds, starting in 3
        InvokeRepeating("Spawn", 3, 4);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void Spawn() {
        // x position between left & right border
        int x = (int)Random.Range(BorderLeft.position.x, BorderRight.position.x);

        // y position between top & bottom border
        int y = (int)Random.Range(BorderBottom.position.y, BorderTop.position.y);

        //check the Position on any Object
        var checkPosition = Physics.OverlapSphere(new Vector3(x,y,0),2);

        if (checkPosition.Length > 0)
            Spawn();
        //set position, initialize food Object and spawn it on all clients
        var position = new Vector2(x, y);
        GameObject food = (GameObject)Instantiate(foodPrefab, position, Quaternion.identity);
        NetworkServer.Spawn(food);

    }
}

